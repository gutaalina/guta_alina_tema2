function addTokens(input, tokens){
    // Test 1: `input` should be a `string`
    if(typeof(input) != "string") {
        throw new Error("Invalid input");
    }
    // Test 2: `input` should be at least 6 characters long.
    if(input.length < 6) {
        throw new Error("Input should have at least 6 characters");
    }
    
    // Test 3: `tokens` is an array with elements with the following format: `{tokenName: string}`
    if(tokens.every(checkElement) != true) {
        throw new Error("Invalid array format");
    }

    // Test 4: If `input` don't contain any `...` return the initial value of `input`
    var substring = "...";
    if(!input.includes(substring)) {
        return input;
    }

    // Test 5: If `input` contains `...`, replace them with the specific values and return the new `input`
    var newString;
    var resultString = input;
    for(var i = 0; i < tokens.length; i++) {
        newString = "${" + tokens[i]["tokenName"] + "}";
        resultString = resultString.replace(substring, newString);
    }

    return resultString;
}

//function for test 3
function checkElement(element) {
    if(Object.keys(element).length != 1) {
        return false;
    }

    if(element["tokenName"] === undefined) {
        return false;
    }

    if(typeof(element["tokenName"]) != "string") {
        return false;
    }

    return true;
}

const app = {
    addTokens: addTokens
}

module.exports = app;